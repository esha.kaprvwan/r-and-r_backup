import { BaseEntity } from "src/base-entity";
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Merchandise } from "src/merchandise/entities/merchandise.entity";


@Entity('category')
export class Category  {

        @PrimaryGeneratedColumn('uuid')
        @OneToMany(() => Merchandise , (merch:Merchandise)=> merch.category)
        id:string;

        @Column()
        category: string;


}

