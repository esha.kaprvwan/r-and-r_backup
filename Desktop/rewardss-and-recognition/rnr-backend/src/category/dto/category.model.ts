import { MerchandiseDTO } from "src/merchandise/dto/merchandise.model";

export class CategoryDTO{
    id?: string;
    category:string;
    createdAt?:string;
    updatedAt?:string;
}