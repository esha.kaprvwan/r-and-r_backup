import { Category } from "../entities/category.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CategoryDTO } from "../dto/category.model";

@Injectable()
export class CategoryService{

    constructor(
        @InjectRepository(Category)
        private CategoryRepository: Repository<CategoryDTO>
    ) { }

    async findAll():Promise<CategoryDTO[]>{
        const categories= await this.CategoryRepository.find().then(result=>{
            return result;
        })
        return categories;
    }

    createCategory(categoryDTO: CategoryDTO) {
        return this.CategoryRepository.save(categoryDTO);
    }
    async findOne(categoryId : string) : Promise<CategoryDTO>{
        const category = await this.CategoryRepository.findOne(categoryId);
        return category;

    }

    

}