import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import  config  from './orm.config';
import { UserModule } from './users/users.module';
import { ConfigModule } from '@nestjs/config';
import { MerchandiseModule } from './merchandise/merchandise.module';
import { CategoryModule } from './category/category.module';
import { SizeModule } from './size/size.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(config),
     UserModule,
     MerchandiseModule,
     CategoryModule,
     SizeModule
  ],
})
export class AppModule {}
