import { Size } from "../entities/size.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { SizeDTO } from "../dto/size.model";

@Injectable()
export class SizeService{

    constructor(
        @InjectRepository(Size)
        private sizeRepository: Repository<SizeDTO>
    ) { }

    async findAll():Promise<SizeDTO[]>{
        const sizes= await this.sizeRepository.find().then(result=>{
            return result;
        })
        return sizes;
    }

    createSize(sizeDTO: SizeDTO) {
        return this.sizeRepository.save(sizeDTO);
    }
    findOne(sizeId : string) : Promise<SizeDTO>{
        return this.sizeRepository.findOne(sizeId);
        

    } 
    

}