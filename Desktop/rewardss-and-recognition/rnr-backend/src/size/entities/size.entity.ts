import { BaseEntity } from "src/base-entity";
import { Entity, Column, OneToMany, JoinColumn, PrimaryGeneratedColumn ,ManyToOne } from "typeorm";
import { Merchandise } from "src/merchandise/entities/merchandise.entity";

@Entity('Size')
export class Size{

        @PrimaryGeneratedColumn("uuid")
        id: string;
      

        @Column({ type: 'varchar', length: 3, nullable: false })
        size: String;


};

