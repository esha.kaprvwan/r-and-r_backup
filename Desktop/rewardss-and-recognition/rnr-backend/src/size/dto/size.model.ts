import { MerchandiseDTO } from "src/merchandise/dto/merchandise.model";

export enum Sizes {
    S = 'S',
    M = 'M',
    L = 'L',
    XL ='XL',
    XXL ='XXL'
  }

export class SizeDTO{
    id?: string;
    size:Sizes;
    createdAt?:string;
    updatedAt?:string;
}