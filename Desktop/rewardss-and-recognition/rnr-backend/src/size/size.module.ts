import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Size } from './entities/size.entity';
import { SizeController } from './controllers/size.controller';
import { SizeService } from './services/size.service';
@Module({
    imports:[TypeOrmModule.forFeature([Size])],
    controllers :[SizeController],
    providers :[SizeService],
    exports : [SizeService]
})
export class SizeModule {}
