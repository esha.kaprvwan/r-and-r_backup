import { SizeDTO } from "src/size/dto/size.model";
import { CategoryDTO } from "src/category/dto/category.model";

export class MerchandiseDTO{
    id?: string;
    title:string;
    count:number;
    points:number;
    sizes:  SizeDTO[]; 
    description:string;
    url:string;
    category: CategoryDTO;   
    createdAt:string;
    updatedAt:string;
};