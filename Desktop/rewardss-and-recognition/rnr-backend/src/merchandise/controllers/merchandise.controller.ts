import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import { query } from "express";
import { CategoryDTO } from "src/category/dto/category.model";
import { CategoryService } from "src/category/services/category.service";
import { MerchandiseDTO } from "../dto/merchandise.model";
import { MerchandiseService } from "../services/merchandise.service";
import { SizeService } from "src/size/services/size.service";
import { SizeDTO } from "src/size/dto/size.model";
import { async } from "rxjs";

@Controller('merchandise')
export class MerchandiseController {
    constructor(private merchandiseService: MerchandiseService,
        private categoryservice: CategoryService,
        private sizeService: SizeService) { }

    @Get()
    async getAllMerchandise() {
        const merchandise = await this.merchandiseService.findAll().then(result => {
            return result;
        }).catch((err) => {
            return ("Some error occured: " + err);
        })
        console.log(merchandise);
        return merchandise;
    }
    @Get('getone')
    async getOneMerchandise(@Query() query: { id: string }) {
        const merchandise = await this.merchandiseService.findOne(query.id);
        return merchandise;
    }

    @Post()
    async addMerchandise(@Body() merch: MerchandiseDTO) {

        let { category, sizes, ...merchandise } = merch;
        console.log(sizes)
        const categoryId = await this.categoryservice.findOne(category.toString());
        const sizedata: SizeDTO[] = []
        let size=[]
        for await (let sizeId of sizes) {
           let data=await this.sizeService.findOne(sizeId.toString());
           sizedata.push(data);
           size.push(data.size);
            
        }


        const { title, url, points}= await this.merchandiseService.createMerchandise({ ...merchandise, sizes: sizedata, category: categoryId });
        
        return {
            title: title,
            url: url,
            sizes: size,
            points: points
        }


    }
}