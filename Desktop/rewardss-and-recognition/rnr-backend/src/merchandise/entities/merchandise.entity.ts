import { BaseEntity } from "src/base-entity"
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne } from "typeorm";
import { Category } from "src/category/entities/category.entity";
import { Size } from "src/size/entities/size.entity";
@Entity('Merchandise')
export class Merchandise extends BaseEntity {

        @Column({ type: 'varchar', length: 100, nullable: false })
        title: string

        @Column({ type : 'int' , nullable:false})
        count: number

        @Column({ type :'int' , nullable:false})
        points: number

        @ManyToMany(() => Size , (size:Size)=> size.id )
        @JoinTable()
        sizes: Size[]

        @Column({ type: 'varchar' , nullable: true})
        description: string

        @Column({ type: "varchar", nullable: false })
        url: string;

        @ManyToOne(() => Category , (category:Category)=> category.id)
        @JoinColumn()                                                    //foreign key
        category: Category

}

