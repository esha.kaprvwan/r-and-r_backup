import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryModule } from 'src/category/category.module';
import { MerchandiseController } from './controllers/merchandise.controller';
import { Merchandise } from './entities/merchandise.entity';
import { MerchandiseService } from './services/merchandise.service';
import { SizeModule } from 'src/size/size.module';

@Module({
    imports:[TypeOrmModule.forFeature([Merchandise]) , CategoryModule , SizeModule],
    controllers :[MerchandiseController],
    providers :[MerchandiseService]
})
export class MerchandiseModule {}
