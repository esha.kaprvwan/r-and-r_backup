import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Category } from "src/category/entities/category.entity";
import { Repository } from "typeorm";
import { MerchandiseDTO } from "../dto/merchandise.model";
import { Merchandise } from "../entities/merchandise.entity";

@Injectable()
export class MerchandiseService {

    constructor(
        @InjectRepository(Merchandise)
        private merchandiseRepository: Repository<Merchandise>
    ) { }

    async findAll():Promise<Merchandise[]>{
        const merchandise= await this.merchandiseRepository.find().then(result=>{
            return result;
        })
        return merchandise;
    }

    createMerchandise(merchandiseDTO: MerchandiseDTO) {
        
        return this.merchandiseRepository.save(merchandiseDTO);
    }

    async findOne(merchId : string) : Promise<Merchandise>{
        const merchandise = await this.merchandiseRepository.findOne(merchId);
        return merchandise;

    }
    
}
