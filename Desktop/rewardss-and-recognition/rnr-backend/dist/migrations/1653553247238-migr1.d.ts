import { MigrationInterface, QueryRunner } from "typeorm";
export declare class migr11653553247238 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
