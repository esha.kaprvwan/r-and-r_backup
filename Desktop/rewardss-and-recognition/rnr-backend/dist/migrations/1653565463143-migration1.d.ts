import { MigrationInterface, QueryRunner } from "typeorm";
export declare class migration11653565463143 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
