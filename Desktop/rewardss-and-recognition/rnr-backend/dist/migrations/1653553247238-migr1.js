"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.migr11653553247238 = void 0;
class migr11653553247238 {
    constructor() {
        this.name = 'migr11653553247238';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "category" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), "category" character varying NOT NULL, CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Size" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "size" character varying(3) NOT NULL, CONSTRAINT "PK_2a370d6ce0ec366a420059489e5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Merchandise" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), "title" character varying(100) NOT NULL, "count" integer NOT NULL, "points" integer NOT NULL, "description" character varying, "categoryId" uuid, CONSTRAINT "REL_aa84e4c3de9eea9e34abef84ea" UNIQUE ("categoryId"), CONSTRAINT "PK_019351f9003428631a70caa0b81" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "public"."users_role_enum" AS ENUM('Admin', 'Appreciator', 'User')`);
        await queryRunner.query(`CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), "name" character varying NOT NULL, "email" character varying NOT NULL, "role" "public"."users_role_enum" NOT NULL DEFAULT 'User', "points" integer NOT NULL, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "merchandise_sizes_size" ("merchandiseId" uuid NOT NULL, "sizeId" uuid NOT NULL, CONSTRAINT "PK_9d6931a1342c59e59d1f63d8904" PRIMARY KEY ("merchandiseId", "sizeId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_69b52856e9b12346bc528b208d" ON "merchandise_sizes_size" ("merchandiseId") `);
        await queryRunner.query(`CREATE INDEX "IDX_54a0085af28b17c436205905e1" ON "merchandise_sizes_size" ("sizeId") `);
        await queryRunner.query(`ALTER TABLE "Merchandise" ADD CONSTRAINT "FK_aa84e4c3de9eea9e34abef84ea0" FOREIGN KEY ("categoryId") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "merchandise_sizes_size" ADD CONSTRAINT "FK_69b52856e9b12346bc528b208da" FOREIGN KEY ("merchandiseId") REFERENCES "Merchandise"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "merchandise_sizes_size" ADD CONSTRAINT "FK_54a0085af28b17c436205905e15" FOREIGN KEY ("sizeId") REFERENCES "Size"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "merchandise_sizes_size" DROP CONSTRAINT "FK_54a0085af28b17c436205905e15"`);
        await queryRunner.query(`ALTER TABLE "merchandise_sizes_size" DROP CONSTRAINT "FK_69b52856e9b12346bc528b208da"`);
        await queryRunner.query(`ALTER TABLE "Merchandise" DROP CONSTRAINT "FK_aa84e4c3de9eea9e34abef84ea0"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_54a0085af28b17c436205905e1"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_69b52856e9b12346bc528b208d"`);
        await queryRunner.query(`DROP TABLE "merchandise_sizes_size"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP TYPE "public"."users_role_enum"`);
        await queryRunner.query(`DROP TABLE "Merchandise"`);
        await queryRunner.query(`DROP TABLE "Size"`);
        await queryRunner.query(`DROP TABLE "category"`);
    }
}
exports.migr11653553247238 = migr11653553247238;
//# sourceMappingURL=1653553247238-migr1.js.map