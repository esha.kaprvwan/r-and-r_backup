"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.merchTable1653387701346 = void 0;
class merchTable1653387701346 {
    constructor() {
        this.name = 'merchTable1653387701346';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "category" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), "category" character varying NOT NULL, CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Size" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "size" character varying(3) NOT NULL, "merch_id" uuid, CONSTRAINT "PK_2a370d6ce0ec366a420059489e5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Merchandise" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), "title" character varying(100) NOT NULL, "count" integer NOT NULL, "points" integer NOT NULL, "description" character varying, "categoryId" uuid, CONSTRAINT "REL_aa84e4c3de9eea9e34abef84ea" UNIQUE ("categoryId"), CONSTRAINT "PK_019351f9003428631a70caa0b81" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "public"."users_role_enum" AS ENUM('Admin', 'Appreciator', 'User')`);
        await queryRunner.query(`CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), "name" character varying NOT NULL, "email" character varying NOT NULL, "role" "public"."users_role_enum" NOT NULL DEFAULT 'User', "points" integer NOT NULL, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "Size" ADD CONSTRAINT "FK_c589d9065ff1baa88e76330806a" FOREIGN KEY ("merch_id") REFERENCES "Merchandise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Merchandise" ADD CONSTRAINT "FK_aa84e4c3de9eea9e34abef84ea0" FOREIGN KEY ("categoryId") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "Merchandise" DROP CONSTRAINT "FK_aa84e4c3de9eea9e34abef84ea0"`);
        await queryRunner.query(`ALTER TABLE "Size" DROP CONSTRAINT "FK_c589d9065ff1baa88e76330806a"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP TYPE "public"."users_role_enum"`);
        await queryRunner.query(`DROP TABLE "Merchandise"`);
        await queryRunner.query(`DROP TABLE "Size"`);
        await queryRunner.query(`DROP TABLE "category"`);
    }
}
exports.merchTable1653387701346 = merchTable1653387701346;
//# sourceMappingURL=1653387701346-merchTable.js.map