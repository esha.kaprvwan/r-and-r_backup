import { MigrationInterface, QueryRunner } from "typeorm";
export declare class migr41653392024188 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
