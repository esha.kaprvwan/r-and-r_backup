import { MigrationInterface, QueryRunner } from "typeorm";
export declare class migr21653390269559 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
