import { MigrationInterface, QueryRunner } from "typeorm";
export declare class migration21653557684618 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
