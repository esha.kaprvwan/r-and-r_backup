"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.migr41653392024188 = void 0;
class migr41653392024188 {
    constructor() {
        this.name = 'migr41653392024188';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "Size" DROP CONSTRAINT "FK_c589d9065ff1baa88e76330806a"`);
        await queryRunner.query(`ALTER TABLE "Size" DROP COLUMN "merch_id"`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "Size" ADD "merch_id" uuid`);
        await queryRunner.query(`ALTER TABLE "Size" ADD CONSTRAINT "FK_c589d9065ff1baa88e76330806a" FOREIGN KEY ("merch_id") REFERENCES "Merchandise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
}
exports.migr41653392024188 = migr41653392024188;
//# sourceMappingURL=1653392024188-migr4.js.map