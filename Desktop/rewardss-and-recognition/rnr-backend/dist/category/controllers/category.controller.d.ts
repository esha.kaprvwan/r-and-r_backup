import { CategoryDTO } from "../dto/category.model";
import { CategoryService } from "../services/category.service";
export declare class CategoryController {
    private categoryService;
    constructor(categoryService: CategoryService);
    getAllCategory(): Promise<string | CategoryDTO[]>;
    getCategory(query: {
        id: string;
    }): Promise<CategoryDTO>;
    addCategory(category: CategoryDTO): Promise<{
        data: CategoryDTO;
    }>;
}
