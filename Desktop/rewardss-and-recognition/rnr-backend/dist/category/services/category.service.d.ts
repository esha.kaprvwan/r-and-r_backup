import { Repository } from "typeorm";
import { CategoryDTO } from "../dto/category.model";
export declare class CategoryService {
    private CategoryRepository;
    constructor(CategoryRepository: Repository<CategoryDTO>);
    findAll(): Promise<CategoryDTO[]>;
    createCategory(categoryDTO: CategoryDTO): Promise<CategoryDTO>;
    findOne(categoryId: string): Promise<CategoryDTO>;
}
