"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'Esha@2001',
    database: 'R_and_R',
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: true,
    migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
    cli: {
        migrationsDir: 'src/migrations',
        entitiesDir: 'src/**/*.entity{ .ts,.js}'
    },
    autoLoadEntities: true,
};
exports.default = config;
//# sourceMappingURL=orm.config.js.map