import { SizeDTO } from "../dto/size.model";
import { SizeService } from "../services/size.service";
export declare class SizeController {
    private SizeService;
    constructor(SizeService: SizeService);
    getAllSize(): Promise<string | SizeDTO[]>;
    getOneSize(query: {
        id: string;
    }): Promise<SizeDTO>;
    addSize(size: SizeDTO): Promise<{
        data: SizeDTO;
    }>;
}
