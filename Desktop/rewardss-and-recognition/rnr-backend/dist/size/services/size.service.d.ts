import { Repository } from "typeorm";
import { SizeDTO } from "../dto/size.model";
export declare class SizeService {
    private sizeRepository;
    constructor(sizeRepository: Repository<SizeDTO>);
    findAll(): Promise<SizeDTO[]>;
    createSize(sizeDTO: SizeDTO): Promise<SizeDTO>;
    findOne(sizeId: string): Promise<SizeDTO>;
}
