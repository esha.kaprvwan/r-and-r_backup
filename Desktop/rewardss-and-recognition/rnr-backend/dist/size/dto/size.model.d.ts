export declare enum Sizes {
    S = "S",
    M = "M",
    L = "L",
    XL = "XL",
    XXL = "XXL"
}
export declare class SizeDTO {
    id?: string;
    size: Sizes;
    createdAt?: string;
    updatedAt?: string;
}
