"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SizeDTO = exports.Sizes = void 0;
var Sizes;
(function (Sizes) {
    Sizes["S"] = "S";
    Sizes["M"] = "M";
    Sizes["L"] = "L";
    Sizes["XL"] = "XL";
    Sizes["XXL"] = "XXL";
})(Sizes = exports.Sizes || (exports.Sizes = {}));
class SizeDTO {
}
exports.SizeDTO = SizeDTO;
//# sourceMappingURL=size.model.js.map