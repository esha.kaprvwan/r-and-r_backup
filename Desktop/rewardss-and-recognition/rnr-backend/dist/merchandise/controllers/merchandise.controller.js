"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchandiseController = void 0;
const common_1 = require("@nestjs/common");
const category_service_1 = require("../../category/services/category.service");
const merchandise_model_1 = require("../dto/merchandise.model");
const merchandise_service_1 = require("../services/merchandise.service");
const size_service_1 = require("../../size/services/size.service");
let MerchandiseController = class MerchandiseController {
    constructor(merchandiseService, categoryservice, sizeService) {
        this.merchandiseService = merchandiseService;
        this.categoryservice = categoryservice;
        this.sizeService = sizeService;
    }
    async getAllMerchandise() {
        const merchandise = await this.merchandiseService.findAll().then(result => {
            return result;
        }).catch((err) => {
            return ("Some error occured: " + err);
        });
        console.log(merchandise);
        return merchandise;
    }
    async getOneMerchandise(query) {
        const merchandise = await this.merchandiseService.findOne(query.id);
        return merchandise;
    }
    async addMerchandise(merch) {
        var e_1, _a;
        let { category, sizes } = merch, merchandise = __rest(merch, ["category", "sizes"]);
        console.log(sizes);
        const categoryId = await this.categoryservice.findOne(category.toString());
        const sizedata = [];
        let size = [];
        try {
            for (var sizes_1 = __asyncValues(sizes), sizes_1_1; sizes_1_1 = await sizes_1.next(), !sizes_1_1.done;) {
                let sizeId = sizes_1_1.value;
                let data = await this.sizeService.findOne(sizeId.toString());
                sizedata.push(data);
                size.push(data.size);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (sizes_1_1 && !sizes_1_1.done && (_a = sizes_1.return)) await _a.call(sizes_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        const { title, url, points } = await this.merchandiseService.createMerchandise(Object.assign(Object.assign({}, merchandise), { sizes: sizedata, category: categoryId }));
        return {
            title: title,
            url: url,
            sizes: size,
            points: points
        };
    }
};
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MerchandiseController.prototype, "getAllMerchandise", null);
__decorate([
    (0, common_1.Get)('getone'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MerchandiseController.prototype, "getOneMerchandise", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [merchandise_model_1.MerchandiseDTO]),
    __metadata("design:returntype", Promise)
], MerchandiseController.prototype, "addMerchandise", null);
MerchandiseController = __decorate([
    (0, common_1.Controller)('merchandise'),
    __metadata("design:paramtypes", [merchandise_service_1.MerchandiseService,
        category_service_1.CategoryService,
        size_service_1.SizeService])
], MerchandiseController);
exports.MerchandiseController = MerchandiseController;
//# sourceMappingURL=merchandise.controller.js.map