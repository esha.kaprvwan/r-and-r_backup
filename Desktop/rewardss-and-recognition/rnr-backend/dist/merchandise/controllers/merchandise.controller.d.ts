import { CategoryService } from "src/category/services/category.service";
import { MerchandiseDTO } from "../dto/merchandise.model";
import { MerchandiseService } from "../services/merchandise.service";
import { SizeService } from "src/size/services/size.service";
export declare class MerchandiseController {
    private merchandiseService;
    private categoryservice;
    private sizeService;
    constructor(merchandiseService: MerchandiseService, categoryservice: CategoryService, sizeService: SizeService);
    getAllMerchandise(): Promise<string | import("../entities/merchandise.entity").Merchandise[]>;
    getOneMerchandise(query: {
        id: string;
    }): Promise<import("../entities/merchandise.entity").Merchandise>;
    addMerchandise(merch: MerchandiseDTO): Promise<{
        title: string;
        url: string;
        sizes: any[];
        points: number;
    }>;
}
