import { Repository } from "typeorm";
import { MerchandiseDTO } from "../dto/merchandise.model";
import { Merchandise } from "../entities/merchandise.entity";
export declare class MerchandiseService {
    private merchandiseRepository;
    constructor(merchandiseRepository: Repository<Merchandise>);
    findAll(): Promise<Merchandise[]>;
    createMerchandise(merchandiseDTO: MerchandiseDTO): Promise<MerchandiseDTO & Merchandise>;
    findOne(merchId: string): Promise<Merchandise>;
}
