import { BaseEntity } from "src/base-entity";
import { Category } from "src/category/entities/category.entity";
import { Size } from "src/size/entities/size.entity";
export declare class Merchandise extends BaseEntity {
    title: string;
    count: number;
    points: number;
    sizes: Size[];
    description: string;
    url: string;
    category: Category;
}
