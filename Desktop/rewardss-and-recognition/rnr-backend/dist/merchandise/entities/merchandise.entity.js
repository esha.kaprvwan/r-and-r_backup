"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Merchandise = void 0;
const base_entity_1 = require("../../base-entity");
const typeorm_1 = require("typeorm");
const category_entity_1 = require("../../category/entities/category.entity");
const size_entity_1 = require("../../size/entities/size.entity");
let Merchandise = class Merchandise extends base_entity_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 100, nullable: false }),
    __metadata("design:type", String)
], Merchandise.prototype, "title", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'int', nullable: false }),
    __metadata("design:type", Number)
], Merchandise.prototype, "count", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'int', nullable: false }),
    __metadata("design:type", Number)
], Merchandise.prototype, "points", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => size_entity_1.Size, (size) => size.id),
    (0, typeorm_1.JoinTable)(),
    __metadata("design:type", Array)
], Merchandise.prototype, "sizes", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], Merchandise.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: "varchar", nullable: false }),
    __metadata("design:type", String)
], Merchandise.prototype, "url", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => category_entity_1.Category, (category) => category.id),
    (0, typeorm_1.JoinColumn)(),
    __metadata("design:type", category_entity_1.Category)
], Merchandise.prototype, "category", void 0);
Merchandise = __decorate([
    (0, typeorm_1.Entity)('Merchandise')
], Merchandise);
exports.Merchandise = Merchandise;
//# sourceMappingURL=merchandise.entity.js.map