"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchandiseModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const category_module_1 = require("../category/category.module");
const merchandise_controller_1 = require("./controllers/merchandise.controller");
const merchandise_entity_1 = require("./entities/merchandise.entity");
const merchandise_service_1 = require("./services/merchandise.service");
const size_module_1 = require("../size/size.module");
let MerchandiseModule = class MerchandiseModule {
};
MerchandiseModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([merchandise_entity_1.Merchandise]), category_module_1.CategoryModule, size_module_1.SizeModule],
        controllers: [merchandise_controller_1.MerchandiseController],
        providers: [merchandise_service_1.MerchandiseService]
    })
], MerchandiseModule);
exports.MerchandiseModule = MerchandiseModule;
//# sourceMappingURL=merchandise.module.js.map