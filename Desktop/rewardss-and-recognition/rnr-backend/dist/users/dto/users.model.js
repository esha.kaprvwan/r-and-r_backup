"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersDTO = exports.Roles = void 0;
var Roles;
(function (Roles) {
    Roles["Admin"] = "Admin";
    Roles["Appreciator"] = "Appreciator";
    Roles["User"] = "User";
})(Roles = exports.Roles || (exports.Roles = {}));
class UsersDTO {
}
exports.UsersDTO = UsersDTO;
//# sourceMappingURL=users.model.js.map