export declare enum Roles {
    Admin = "Admin",
    Appreciator = "Appreciator",
    User = "User"
}
export declare class UsersDTO {
    id: string;
    name: string;
    email: string;
    role: string[];
    points: number;
}
