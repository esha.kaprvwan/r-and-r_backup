import { UsersDTO } from '../dto/users.model';
import { UsersService } from '../services/users.services';
export declare class UsersController {
    private userService;
    constructor(userService: UsersService);
    appendUser(user: UsersDTO): Promise<UsersDTO & import("../entities/users.entity").Users>;
    getUser(): Promise<import("../entities/users.entity").Users[]>;
}
