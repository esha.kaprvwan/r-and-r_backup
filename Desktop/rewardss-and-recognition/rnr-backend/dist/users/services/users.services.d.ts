import { Repository } from 'typeorm';
import { UsersDTO } from '../dto/users.model';
import { Users } from '../entities/users.entity';
export declare class UsersService {
    private usersRepository;
    constructor(usersRepository: Repository<Users>);
    createUser(userDTO: UsersDTO): Promise<UsersDTO & Users>;
    getUsers(): Promise<Users[]>;
}
