import { BaseEntity } from 'src/base-entity';
export declare class Users extends BaseEntity {
    name: string;
    email: string;
    role: string[];
    points: number;
}
